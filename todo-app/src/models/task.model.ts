export class Task {
  public id: number;
  public label: string;
  public description: string;
  public category: string;
  public done: boolean | string;
  
  constructor () {
    this.label = '';
    this.description = '';
    this.category = 'default';
    this.done = false;
  }
}