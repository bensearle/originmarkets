import { EventEmitter, Injectable } from '@angular/core';
import { Task } from 'src/models/task.model';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private _tasks: Task[];
  onChange: EventEmitter<Task[]> = new EventEmitter();

  constructor(private api: ApiService) {
    this.refresh();
    this.api.onListChange.subscribe(() => { this.refresh(); });
  }

  get tasks(): Task[] {
    return this._tasks ? JSON.parse(JSON.stringify(this._tasks)) : null;
  }

  get categories(): string[] {
    return [
      'default',
      'bureaucracy',
      'health',
      'house',
      'social',
      'work',
      'other'
    ]
  }

  getFirstTask(notEqualToId?: number): Task {
    for (let task of this._tasks) {
      if (!notEqualToId || task.id !== notEqualToId) {
        return task;
      }
    }
    return null;
  }

  private refresh() {
    this.api.getTasks()
      .then(items => {
        this._tasks = items.sort((a, b) => {
          if (a.done === b.done) return b.id - a.id;
          return (!a.done || (b.done && a.done > b.done)) ? -1 : 1;
        });
        this.onChange.emit(this.tasks);
      })
      .catch(error => {
        console.error(error);
      });
  }
}

