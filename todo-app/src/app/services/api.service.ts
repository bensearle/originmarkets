import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Task } from 'src/models/task.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  baseUrl = 'http://localhost:3000/tasks';
  onListChange: EventEmitter<any> = new EventEmitter();

  constructor(private http: HttpClient) { }

  getTasks(id?: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(this.baseUrl)
        .subscribe(
          response => {
            resolve(response);
          },
          error => {
            reject(error);
          }
        );
    })
  }

  createTask(task: Task): Promise<any>  {
    return new Promise((resolve, reject) => {
      this.http.post(this.baseUrl, task)
        .subscribe(
          response => {
            resolve(response);
            this.onListChange.emit();
          },
          error => {
            reject(error);
          }
        );
    })
  }

  editTask(task: Task): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.patch(`${this.baseUrl}/${task.id}`, task)
        .subscribe(
          response => {
            resolve(response);
            this.onListChange.emit();
          },
          error => {
            reject(error);
          }
        );
    })
  }

  deleteTask(task: Task): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.delete(`${this.baseUrl}/${task.id}`)
        .subscribe(
          response => {
            resolve(response);
            this.onListChange.emit();
          },
          error => {
            reject(error);
          }
        );
    })
  }

}
