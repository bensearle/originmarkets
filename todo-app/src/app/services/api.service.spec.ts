import { TestBed, inject } from '@angular/core/testing';

import { ApiService } from './api.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Task } from 'src/models/task.model';

const testData: Task[] = [
  {
    "id": 1,
    "label": "Kitchen Cleanup",
    "description": "Clean my dirty kitchen",
    "category": "house",
    "done": false
  },
  {
    "id": 2,
    "label": "Taxes",
    "description": "Start doing my taxes and contact my accountant jhon for advice",
    "category": "bureaucracy",
    "done": "22-10-2019"
  }
];

describe('ApiService', () => {
  let service: ApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
    service = TestBed.inject(ApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get all tasks',
    inject([HttpTestingController, ApiService],
      (httpMock: HttpTestingController, service: ApiService) => {
        service.getTasks()
          .then(data => {
            expect(data.length).toBe(2);
            expect(data[0].id).toEqual(1);
            expect(data[0].label).toEqual('Kitchen Cleanup');
            expect(data[0].description).toEqual('Clean my dirty kitchen');
            expect(data[0].category).toEqual('house');
            expect(data[0].done).toEqual(false);
          })
          .catch(error => {
            expect(false);
          })
        const req = httpMock.expectOne(`${service.baseUrl}`);
        expect(req.request.method).toEqual('GET');
        req.flush(testData);
      })
  );

  it('should create a task',
    inject([HttpTestingController, ApiService],
      (httpMock: HttpTestingController, service: ApiService) => {
        let task = JSON.parse(JSON.stringify(testData[0]));
        delete task.id;
        service.createTask(task)
          .then(data => {
            expect(true);
            expect(data.id).toEqual(3);
          })
          .catch(error => {
            expect(false);
          })
        const req = httpMock.expectOne(`${service.baseUrl}`);
        expect(req.request.method).toEqual('POST');
        let response = JSON.parse(JSON.stringify(testData[0]));
        response.id = 3;
        req.flush(response);
      })
  );

  it('should edit a task',
    inject([HttpTestingController, ApiService],
      (httpMock: HttpTestingController, service: ApiService) => {
        let task = JSON.parse(JSON.stringify(testData[0]));
        task.description = 'Clean my dirty kitchen\nDo dishes';
        service.editTask(task)
          .then(data => {
            expect(true);
            expect(data.id).toEqual(1);
            expect(data.description).toEqual('Clean my dirty kitchen\nDo dishes');
          })
          .catch(error => {
            expect(false);
          })
        const req = httpMock.expectOne(`${service.baseUrl}/${task.id}`);
        expect(req.request.method).toEqual('PATCH');
        let response = JSON.parse(JSON.stringify(testData[0]));
        response.description = 'Clean my dirty kitchen\nDo dishes';
        req.flush(response);
      })
  );

  it('should delete a task',
    inject([HttpTestingController, ApiService],
      (httpMock: HttpTestingController, service: ApiService) => {
        let task = JSON.parse(JSON.stringify(testData[0]));
        service.deleteTask(task)
          .then(data => {
            expect(data).toEqual({});
          })
          .catch(error => {
            expect(false);
          })
        const req = httpMock.expectOne(`${service.baseUrl}/${task.id}`);
        expect(req.request.method).toEqual('DELETE');
        req.flush({});
      })
  );

});
