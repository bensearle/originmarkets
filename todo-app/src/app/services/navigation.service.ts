import { Injectable, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Task } from 'src/models/task.model';
import { DataService } from './data.service';
import { Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {
  _selectedTask: number | 'new' | null; // id, new note, closed
  onChangeTask: EventEmitter<number | 'new' | null> = new EventEmitter();
  initialDataSubscription: Subscription;

  constructor(private data: DataService, private router: Router) {
    let onInitialData = () => {
      this.routeTaskDetail(this.data.getFirstTask());
      if (this.initialDataSubscription) {
        this.initialDataSubscription.unsubscribe();
        this.initialDataSubscription = null;
      }
    }

    this.data.tasks ? onInitialData() : this.initialDataSubscription = this.data.onChange.subscribe(onInitialData);
  }

  /**
   * task id, 'new' note, null=closed
   */
  get selectedTask(): number | 'new' | null {
    return this._selectedTask;
  }

  updateSelectedTask(id: number) {
    if (this._selectedTask === id) return;
    this._selectedTask = id;
    this.onChangeTask.emit(this.selectedTask);
  }

  to = {
    /**
     * navigate to task
     * @param task
     */
    task: (taskData: Task) => { this.routeTaskDetail(taskData); },
    /**
     * navigate to a new task
     */
    newTask: () => { this.routeTaskDetail(); }
  }

  close = {
    /**
     * navigate away from a task
     */
    task: (id?: number) => {
      this.routeTaskDetail(this.data.getFirstTask(id));
    },
  }

  private routeTaskDetail(taskData?: Task) {
    if (taskData && taskData.id === this._selectedTask) return;
    this._selectedTask = taskData ? taskData.id : 'new';
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      // this.router.navigate([task ? `task/${task.id}` : ``], { state: task });
      this.router.navigate(['task'], { state: taskData });
      this.onChangeTask.emit(this.selectedTask);
    });
  }

}
