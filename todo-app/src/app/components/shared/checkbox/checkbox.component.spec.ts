import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckboxComponent } from './checkbox.component';

describe('CheckboxComponent', () => {
  let component: CheckboxComponent;
  let fixture: ComponentFixture<CheckboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CheckboxComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change css class based on input', () => {
    component.state = true;
    fixture.detectChanges();
    expect(fixture.nativeElement.firstChild.className).toContain('checked');
    component.state = false;
    fixture.detectChanges();
    expect(fixture.nativeElement.firstChild.className).not.toContain('checked');
  });

  it('should toggle when clicked', () => {
    component.state = false;
    fixture.detectChanges();
    spyOn(component.stateChange, 'emit');
    fixture.nativeElement.firstChild.click();
    fixture.detectChanges();
    expect(fixture.nativeElement.firstChild.className).toContain('checked'); 
    expect(component.stateChange.emit).toHaveBeenCalledWith(true);
  });

});
