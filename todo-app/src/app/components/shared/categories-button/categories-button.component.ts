import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-categories-button',
  templateUrl: './categories-button.component.html',
  styleUrls: ['./categories-button.component.scss']
})
export class CategoriesButtonComponent implements OnInit {
  @Input() selected: string;
  @Input() light: boolean;
  @Input() filter: boolean;
  @Output() selectedChange: EventEmitter<string> = new EventEmitter<string>();
  isLight: boolean;
  isFilter: boolean;
  categories: string[];

  constructor(data: DataService) { 
    this.categories = data.categories;
  }

  ngOnInit(): void {
    this.isLight = typeof this.light !== 'undefined';
    this.isFilter = typeof this.filter !== 'undefined';
  }

  onChange(category) {
    if (category === '0') category = null; 
    if (this.selected === category) return;
    this.selected = category;
    this.selectedChange.emit(this.selected);
  }

}
