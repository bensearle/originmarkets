import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriesButtonComponent } from './categories-button.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('CategoriesButtonComponent', () => {
  let component: CategoriesButtonComponent;
  let fixture: ComponentFixture<CategoriesButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      declarations: [ CategoriesButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriesButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  
});
