import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { CategoriesButtonComponent } from './categories-button/categories-button.component';
import { CheckboxComponent } from './checkbox/checkbox.component';

const modules = [
  CategoriesButtonComponent,
  CheckboxComponent
];

@NgModule({
  imports: [
    HttpClientModule,
    CommonModule
  ],
  declarations: modules,
  exports: modules
})
export class SharedModule { }
