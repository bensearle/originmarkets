import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TaskDetailComponent } from './task-detail/task-detail.component';


const routes: Routes = [
  // { path: '', component: TaskDetailComponent},
  { path: 'task', component: TaskDetailComponent },
  { path: 'task/:id', component: TaskDetailComponent },
  // { path: '**', redirectTo: '/task' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaskViewRoutingModule { }
