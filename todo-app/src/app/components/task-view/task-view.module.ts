import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TaskViewRoutingModule } from './task-view-routing.module';
import { TaskViewComponent } from './task-view.component';
import { TaskDetailComponent } from './task-detail/task-detail.component';
import { TaskListComponent } from './task-list/task-list.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    TaskDetailComponent,
    TaskListComponent,
    TaskViewComponent
  ],
  imports: [
    CommonModule,
    TaskViewRoutingModule,
    SharedModule
  ],
  exports: [
    TaskViewComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TaskViewModule { }
