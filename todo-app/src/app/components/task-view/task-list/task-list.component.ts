import { Component } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { NavigationService } from 'src/app/services/navigation.service';
import { Task } from 'src/models/task.model';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent {
  taskList: Task[];
  selectedId: number;
  filter: null | string;

  constructor(private data: DataService, private navigation: NavigationService) {
    this.refresh();
    this.data.onChange.subscribe(data => { this.refresh(data); })
    this.navigation.onChangeTask.subscribe(id => this.selectedId = id);
  }

  refresh(data?: Task[]) {
    let taskList = data || this.data.tasks || [];
    if (this.filter) {
      taskList = taskList.filter(task => {
        return task.category === this.filter;
      })
    }

    this.taskList = taskList;
  }

  select(task) {
    this.navigation.to.task(task);
  }

  onSelectFilter(category) {
    this.filter = category;
    this.refresh();
  }
}
