import { Component, OnInit } from '@angular/core';
import { NavigationService } from 'src/app/services/navigation.service';

@Component({
  selector: 'app-task-view',
  templateUrl: './task-view.component.html',
  styleUrls: ['./task-view.component.scss']
})
export class TaskViewComponent implements OnInit {
  isTaskOpen;

  constructor(private navigation: NavigationService) { 
    this.isTaskOpen = !!this.navigation.selectedTask;
    this.navigation.onChangeTask.subscribe(selectedTask => this.isTaskOpen = !!selectedTask);
  }

  newTask() {
    this.navigation.to.newTask();
  }

  ngOnInit(): void {
  }

}
