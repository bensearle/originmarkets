import { Component, ViewChild, ElementRef } from '@angular/core';
import { Task } from 'src/models/task.model';
import { ApiService } from 'src/app/services/api.service';
import { NavigationService } from 'src/app/services/navigation.service';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.scss']
})
export class TaskDetailComponent {

  @ViewChild('note') note: ElementRef;
  task: Task;
  lastSavedTask: string;
  noteInitialContent: {
    label: string,
    descriptionArray: string[]
  };
  isDone: boolean;

  constructor(private api: ApiService, private navigation: NavigationService) {
    if (history.state && history.state.id) {
      this.refreshTask(history.state, true);
    } else {
      // new task
      this.task = new Task();
      this.noteInitialContent = {
        label: '',
        descriptionArray: ['']
      }
      this.isDone = false;
    }
  }

  refreshTask(task: Task, isInit?: boolean) {
    if (isInit) {
      this.noteInitialContent = {
        label: task.label,
        descriptionArray: task.description.split(/[\r\n]/g)
      }
      this.isDone = !!task.done;
    } else if (this.task.id !== task.id) {
      this.navigation.updateSelectedTask(task.id);
    }

    this.task = task;
    this.lastSavedTask = JSON.stringify(task);
  }

  ngOnDestroy() {
    // this.save(true);
  }

  ngAfterViewInit() {
    if (!history.state || !history.state.id) {
      setTimeout(() => { this.note.nativeElement.focus(); })
    }
  }

  onDone(state) {
    this.isDone = state;
    this.task.done = state ? formatDate(new Date(), 'dd-MM-yyyy', 'en') : false;
    this.save(true);
  }

  onSelectCategory(category) {
    if (this.task.category === category) return;
    this.task.category = category;
    this.save(true);
  }

  save(isAutoSave?: boolean) {
    if (!this.note) return;

    let text = this.note.nativeElement.innerText.trim().replace(/[\r\n]{2}/g, '\n');
    let index = text.indexOf('\n');
    let input = index !== -1 ? [text.slice(0, index), text.slice(index + 1)] : [text, ''];

    let task = this.task;
    task.label = input[0];
    task.description = input[1];

    if (!task.label && !task.description) {
      if (task.id) this.delete();
      return;
    }
    if (this.lastSavedTask === JSON.stringify(task)) {
      return;
    }

    let onApiSuccess = (t) => {
      this.refreshTask(t);
    }
    if (task.id) {
      this.api.editTask(task).then(onApiSuccess);
    } else {
      this.api.createTask(task).then(onApiSuccess);
    }
  }

  delete() {
    if (this.task.id) {
      this.api.deleteTask(this.task)
        .then(() => {
          this.navigation.close.task(this.task.id);
        })
        .catch();
    } else {
      this.navigation.close.task();
    }
  }

}
